<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UabPayResponseController extends Controller
{
    public function handlePaymentResponse(Request $request)
    {
        // Demo Code Flow
        // 1. Check Valid Hash Value

        // 2. Check Correct Channel
        if ($request["Channel"] !== "TEST UAB") {
            return [
                "RespDescription" => "Invalid Merchant Channel",
                "RespCode" => "905",
                "HashValue" => "E343EDE4A379C44B87511FFA0282EE6CC1AA07AE"
            ];
        }

        // 3. Check Transaction Status Successful

        // 4. Check Correct Sequence No. and InvoiceNo

        // 5. Check Amount with InvoiceNo

        // 6. Store the Invoice with Success Status

        // 7. Return Successful Response
        // 7.1 Calculate Hash Value
        $concatenatedString = "000" . "Success" . "{\"ItemId\":\"Test uab product\",\"Quantity\":\"1\",\"EachPrice\":\"1200.00\"}";
        $concatenatedString .= $request["ReferIntegrationId"] . "Data" . "";

        $hashValue = hash_hmac('sha1', $concatenatedString, "FF8CBAF6E562", false);

        $response = [
            "ReferIntegrationID" => $request["ReferIntegrationId"],
            "DataType" => "Data",
            "ConfrimationUrl" => "",
            "ItemListJsonStr" => "{\"ItemId\":\"Test uab product\",\"Quantity\":\"1\",\"EachPrice\":\"1200.00\"}",
            "RespDescription" => "Success",
            "RespCode" => "000",
            "HashValue" => strtoupper($hashValue)
        ];

        Log::info("uabpay handle Payment Response");
        Log::info($request);
        Log::info($response);

        return $response;
    }
}
